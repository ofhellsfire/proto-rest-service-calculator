var angular = require('angular');

require('./app.service');

angular.module("main").controller('MainController',
    ['$scope','mainService',
    function ($scope, mainService) {
        var vm = this;

        vm.items={operand:[]};
        vm.result = "";
        vm.model = {param : 0};
        vm.addItem = function(){
            vm.items.operand.push(vm.model.param);
        };

        vm.addition = function(){
            mainService.addition(vm.items).then(function(result){
                vm.result = result.data;
            })
        };
        vm.subtraction = function(){
            mainService.subtraction(vm.items).then(function(result){
                vm.result = result.data;
            })
        };
        vm.multiplication = function(){
            mainService.multiplication(vm.items).then(function(result){
                vm.result = result.data;
            })
        };
        vm.division = function(){
            mainService.division(vm.items).then(function(result){
                vm.result = result.data;
            })
        };
}]);
