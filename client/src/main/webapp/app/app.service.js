var angular = require('angular');
require('./app.resources');

angular.module("main").factory("mainService", [
    "$q",
    "$http",
    "CalculationResource",
    function($q, $http, CalculationResource) {
        var addition = function(object) {
            var deferredObject = $q.defer();

            $http.get('rest-test/rest/calculation/addition', {params: object}).then(function(result) {
                deferredObject.resolve({data: result.data, success: true});
            }, function(error) {
                deferredObject.resolve({success: false, message: error});
            });
            return deferredObject.promise;
        };
        var subtraction = function(object) {
            var deferredObject = $q.defer();

            $http.get('rest-test/rest/calculation/subtraction', {params: object}).then(function(result) {
                deferredObject.resolve({data: result.data, success: true});
            }, function(error) {
                deferredObject.resolve({success: false, message: error});
            });
            return deferredObject.promise;
        };

        var multiplication = function(object) {
            var deferredObject = $q.defer();

            $http.get('rest-test/rest/calculation/multiplication', {params: object}).then(function(result) {
                deferredObject.resolve({data: result.data, success: true});
            }, function(error) {
                deferredObject.resolve({success: false, message: error});
            });
            return deferredObject.promise;
        };

        var division = function(object) {
            var deferredObject = $q.defer();

            $http.get('rest-test/rest/calculation/division', {params: object}).then(function(result) {
                deferredObject.resolve({data: result.data, success: true});
            }, function(error) {
                deferredObject.resolve({success: false, message: error});
            });
            return deferredObject.promise;
        };

        return {addition: addition, subtraction: subtraction, multiplication: multiplication, division: division};

    }
]);
