var angular = require('angular');

angular.module("main").factory('CalculationResource', [
    '$resource',
    function($resource) {
        return $resource('rest-test/rest/calculation/subtraction', {}, {
            "query": {
                method: 'GET'
            }
        });
    }
]);
