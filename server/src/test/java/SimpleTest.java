import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SimpleTest {

        @Test
        public void multiplicationOfZeroIntegersShouldReturnZero() {
                // assert statements
                assertEquals("10 x 0 must be 0", 0, 10 * 0);
        }
}
