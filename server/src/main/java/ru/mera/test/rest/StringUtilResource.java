/**
 * Info about this package doing something for package-info.java file.
 */
package ru.mera.test.rest;


import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


/**
*  Documentation.
*/
@Path("/string-util")
@XmlAccessorType(XmlAccessType.NONE)
public class StringUtilResource {

  /**
   * blah-blah-blah.
   *
   * @return value
   */
  @GET
  public final String calculationInfo() {
    return "String util resource v1";
  }

  /**
   * blah-blah-blah.
   *
   * @param string param
   * @return value
   */
  @Path("/upper")
  @Consumes("text/plain")
  @Produces("text/plain")
  @POST
  public final String toUpperCase(final String string) {
    return string.toUpperCase();
  }

  /**
   * blah-blah-blah.
   *
   * @param string param
   * @return value
   */
  @Path("/lower")
  @Consumes("text/plain")
  @Produces("text/plain")
  @POST
  public final String toLowerCase(final String string) {
    return string.toLowerCase();
  }

  /**
   * blah-blah-blah.
   *
   * @param string param
   * @param from param
   * @param to param
   * @return value
   */
  @Path("/substring")
  @Consumes("text/plain")
  @Produces("text/plain")
  @POST
  public final String substring(final String string,
                      @QueryParam("from") @DefaultValue("0") final Integer from,
                      @QueryParam("to") @DefaultValue("-1") final Integer to) {
    if (to < 0) {
      return string.substring(from);
    } else {
      return string.substring(from, to);
    }
  }
}
